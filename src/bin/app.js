const KEY = 'bea636adf6fd619fc75931109710eb47';

const getWeatherInfo = async location => {
    let data = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${location}&APPID=${KEY}`);
    let details = await data.json();
    const weather = details.weather.map(w => w.description);

    return {
        temp: details.main.temp,
        humidity: details.main.humidity,
        wind: details.wind,
        weather,
        location
    };
}

export { getWeatherInfo };