const setWeatherData = data => {
    const tempElement = document.querySelector('#temp');
    const humidityElement = document.querySelector('#humidity');
    const windElement = document.querySelector('#wind'); 
    const weatherElement = document.querySelector('#weather-info');
    
    tempElement.innerHTML = `${parseInt(data.temp)} Deg.`;
    humidityElement.innerHTML = `${parseInt(data.humidity)} %`;
    windElement.innerHTML = `${data.wind.speed} m/s, ${data.wind.deg} Deg`;
    weatherElement.innerHTML = `${data.location} - ${data.weather.join(",")}`;
}

export { setWeatherData };