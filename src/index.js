// app style
require('./main.scss');

import { getWeatherInfo } from './bin/app';
import { setWeatherData } from './bin/dom';

document.addEventListener("DOMContentLoaded", () => {

    // start with default location
    getWeatherInfo("Nairobi")
        .then(info => setWeatherData(info));

    // handle form clicks
    const form = document.querySelector('.search');
    form.addEventListener('submit', e => {
        e.preventDefault();
        const location = document.querySelector('#location').value;

        console.log(location);
        
        getWeatherInfo(location)
            .then(info => setWeatherData(info));
    });
});